using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Audio;

public class EndLastLevel : MonoBehaviour
{
    public AudioSource win;

    public Text levelFinished;

    public GameObject canvas;

    public GameObject transition;


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            if (EndLevel.playerEndLevel == false)
            {
                win.Play();
                levelFinished.gameObject.SetActive(true);
                EndLevel.playerEndLevel = true;
                CoinManager.levelCoinsCount = 0;
                StartCoroutine(LevelEndTransition());
            }
        }
    }

    IEnumerator LevelEndTransition()
    {
        yield return new WaitForSeconds(1.5f);
        PlayerPrefs.SetInt("TotalCoins", CoinManager.globalCoinsCount);
        canvas.gameObject.SetActive(false);
        levelFinished.gameObject.SetActive(false);
        transition.SetActive(true);
        Invoke("ChangeScene", 1f);
    }

    void ChangeScene()
    {
        EndLevel.playerEndLevel = false;
        SceneManager.LoadScene("GameMenu");
    }
}
